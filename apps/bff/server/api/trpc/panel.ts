import { renderTrpcPanel } from "trpc-panel";
import { appRouter } from "~/../../packages/api";

export default defineEventHandler((event) => {
  event.node.res.statusCode = 200;
  event.node.res.end(
    renderTrpcPanel(appRouter, {
      url: "http://localhost:3001/api/trpc",
      transformer: "superjson",
    })
  );
});
