import { createNuxtApiHandler } from "trpc-nuxt";
import { appRouter, createContext } from "@parkos-pbe/api";

// export API handler
export default createNuxtApiHandler({
  router: appRouter,
  createContext,
});
