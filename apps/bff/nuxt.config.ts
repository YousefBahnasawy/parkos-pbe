import config from "@parkos-pbe/configs/nuxt";

// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  ...config,
  modules: ["@nuxt/devtools"],
  devServer: {
    port: 3001,
  },
});
