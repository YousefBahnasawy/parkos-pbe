import plugin from "tailwindcss/plugin";
import type { Config } from "tailwindcss";

export default <Partial<Config>>{
  content: [
    "./components/**/*.{js,vue,ts}",
    "./layouts/**/*.vue",
    "./pages/**/*.vue",
  ],
  theme: {
    screens: {
      xxxs: "321px",
      xxs: "376px",
      xs: "576px",
      sm: "770px",
      md: "990px",
      lg: "1190px",
      xl: "1190px",
      "2xl": "1190px",
    },
    container: {
      center: true,
    },
    extend: {
      zIndex: {
        "-10": "-10",
        "1": "1",
      },
      fontSize: {
        15: "15px",
        sm: "0.75rem",
        md: "0.875rem",
        lg: "1.125rem",
        xl: ["1.25rem", "1.2"],
        "2xl": ["1.625rem", "1.2"],
        "3xl": ["1.875rem", "1.2"],
      },
      transitionProperty: {
        "bg-ease": "background .3s ease",
      },
      spacing: {
        "7/10": "70%",
        8.5: "2.125rem",
        12: "3.125rem",
        18: "4.5rem",
        23: "5.5rem",
        24: "6.25rem",
        26: "6.5rem",
        7.5: "1.875rem",
        3.75: "0.9375rem",
        "10px": "10px",
        "30px": "30px",
        "125px": "125px",
        "83px": "83px",
        "4px": "4px",
        "10rem": "10rem",
      },
      colors: {
        "blaze-orange": {
          50: "#FFD2B8",
          100: "#FFC5A3",
          200: "#FFAB7A",
          300: "#FF9152",
          400: "#FF7729",
          500: "#FF5D00",
          600: "#C74900",
          700: "#8F3400",
          800: "#572000",
          900: "#1F0B00",
        },
        orange: {
          50: "#fef6ee",
          100: "#fdebd7",
          200: "#fbd3ad",
          300: "#f8b479",
          400: "#f38b44",
          500: "#f06b1f",
          600: "#e15215",
          700: "#bb3c13",
          800: "#953117",
          900: "#782a16",
        },
        "blue-alt": {
          DEFAULT: "#AAC6FF",
          50: "#FFFFFF",
          100: "#FFFFFF",
          200: "#FFFFFF",
          300: "#FCFDFF",
          400: "#D3E1FF",
          500: "#AAC6FF",
          600: "#72A0FF",
          700: "#3A7BFF",
          800: "#0255FF",
          900: "#0042C9",
        },
        blue: {
          50: "#eef7ff",
          100: "#d9ebff",
          200: "#bcdeff",
          300: "#8dc9ff",
          400: "#58aaff",
          500: "#3188ff",
          600: "#206bf6",
          700: "#1351e2",
          800: "#1642b7",
          900: "#183b90",
        },
        "chathams-blue": {
          50: "#eef8ff",
          100: "#d8eeff",
          200: "#bae2ff",
          300: "#8bd2ff",
          400: "#54b7ff",
          500: "#2d97ff",
          600: "#1677fa",
          700: "#0f5fe6",
          800: "#134dba",
          900: "#123878",
        },
        gray: {
          50: "#f7f8fa",
          100: "#f3f4f7",
          200: "#e5e7ed",
          300: "#d0d3dc",
          400: "#9ca1b0",
          500: "#6c7183",
          600: "#4e5362",
          700: "#3a3f4e",
          800: "#242735",
          900: "#151724",
        },
        "black-alt": {
          50: "#E7E8EF",
          100: "#CBCEDC",
          200: "#979DBA",
          300: "#646D96",
          400: "#434965",
          500: "#202330",
          600: "#1B1D28",
          700: "#12141C",
          800: "#0C0D12",
          900: "#060709",
        },
        silver: {
          50: "#FFFFFF",
          100: "#F5F5F5",
          200: "#E0E0E0",
          300: "#CCCCCC",
          400: "#B8B8B8",
          500: "#A3A3A3",
          600: "#878787",
          700: "#6B6B6B",
          800: "#4F4F4F",
          900: "#333333",
        },
        athens: {
          DEFAULT: "#E5E7EB",
          50: "#FFFFFF",
          100: "#FFFFFF",
          200: "#FFFFFF",
          300: "#FFFFFF",
          400: "#FCFCFD",
          500: "#E5E7EB",
          600: "#C5CAD3",
          700: "#A6ACBA",
          800: "#868FA2",
          900: "#687387",
        },
        cornflower: {
          DEFAULT: "#6996E9",
          50: "#FFFFFF",
          100: "#F7FAFE",
          200: "#D4E1F9",
          300: "#B0C8F3",
          400: "#8DAFEE",
          500: "#6996E9",
          600: "#3874E2",
          700: "#1D58C5",
          800: "#164294",
          900: "#0F2C63",
        },
        manatee: {
          DEFAULT: "#9797A6",
          50: "#FAFAFB",
          100: "#EFEFF1",
          200: "#D9D9DE",
          300: "#C3C3CC",
          400: "#ADADB9",
          500: "#9797A6",
          600: "#79798C",
          700: "#5E5E6E",
          800: "#454550",
          900: "#2B2B32",
        },
      },
      lineHeight: {
        11: "2.5rem",
        12: "3rem",
        13: "3.5rem",
        relative: "1.875",
      },
      transformOrigin: {
        navTop: "10% 10%",
        navBottom: "10% 90%",
      },
      width: {
        1000: "1000px",
      },
    },
    fontFamily: {
      sans: ['"Open Sans"', "sans-serif"],
      heading: ['"Lato"', "sans-serif"],
    },
    boxShadow: {
      dropdown: "0 6px 12px rgb(0 0 0 / 18%)",
      button: "inset 0 -2px 0 0 rgba(0, 0, 0, 0.2)",
      buttonhover: "inset 0 -2px 0 0 #828282",
      input: "inset 0 3px 0 0 hsla(0,0%,82.4%,.4)",
      section: "0 25px 50px -12px rgb(0 0 0 / 0.25)",
      card: "0 10px 14px 0 rgba(15, 19, 32, 0.02), 0 1px 2px 0 rgba(10, 11, 18, 0.08)",
      sepeartor: "0 1px 0 0 rgba(0, 0, 0, 0.09)",
      icon: "0 8px 8px 0 rgba(16, 23, 43, 0.03), 0 1px 2px 0 rgba(10, 15, 37, 0.14)",
    },
  },
  plugins: [
    plugin(({ addComponents }) => {
      addComponents({
        ".default-padding": {
          paddingLeft: "1.5rem",
          paddingRight: "1.5rem",
          "@media (min-width: 1024px)": {
            paddingLeft: "2.75rem",
            paddingRight: "2.75rem",
          },
          "@media (min-width: 1280px)": {
            paddingLeft: "4.5rem",
            paddingRight: "4.5rem",
          },
          "@media (min-width: 1536px)": {
            paddingLeft: "12rem",
            paddingRight: "12rem",
          },
        },
        ".grey-text": {
          "--tw-text-opacity": "1",
          color: "rgb(130 130 130 / var(--tw-text-opacity))",
        },
      });
    }),
  ],
  ...(process.env.NODE_ENV === "production" ? { cssnano: {} } : {}),
};
