import { NuxtConfig } from "nuxt/config";

export default <NuxtConfig>{
  build: {
    transpile: ["trpc-nuxt"],
  },
  vite: {
    clearScreen: false,
  },
  modules: ["@nuxtjs/tailwindcss", "@nuxt/devtools"],
  typescript: {
    strict: true,
  },
};
