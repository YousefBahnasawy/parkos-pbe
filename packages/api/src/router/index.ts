import { router } from "../trpc";
import { airportsRouter } from "./airports";
import { countryRouter } from "./country";
import { languaugesRouter } from "./languages";

export const appRouter = router({
  airports: airportsRouter,
  country: countryRouter,
  languages: languaugesRouter,
});

// export type definition of API
export type AppRouter = typeof appRouter;
