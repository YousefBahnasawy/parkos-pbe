import { publicProcedure, router } from "../trpc";
import { z } from "zod";
import { TRPCError } from "@trpc/server";

function stripObject(requiredFieldsArray: any[], obj?: any) {
  if (!obj) {
    return {};
  }
  const reparsed = JSON.parse(JSON.stringify(obj));
  return Object.fromEntries(
    Object.entries(reparsed).filter(([k, _v]) =>
      requiredFieldsArray.includes(k)
    )
  );
}

export const airportsRouter = router({
  airports: publicProcedure
    .input(
      z.object({
        lang: z.string(),
        orderBy: z.enum(["asc", "desc"]),
        orderOn: z.string(),
      })
    )
    .query(async ({ input, ctx }) => {
      const language = await ctx.prisma.languages.findFirst({
        select: {
          id: true,
          lang: true,
          name: true,
          native_name: true,
        },
        where: {
          lang: input.lang,
        },
      });

      const locations = await ctx.prisma.locations.findMany({
        select: {
          countries: {
            include: {
              countries_content: {
                where: { lang: language?.id },
              },
            },
          },
          location_statistics: {
            where: {
              language_id: language?.id,
            },
          },
          merchants: {
            select: {
              id: true,
            },
            where: {
              status: 1,
            },
          },
        },
      });

      const locations_content = await ctx.prisma.locations_content.findMany({
        where: {
          lang: language?.id,
          slug: {
            not: "",
          },
        },
      });

      return { ...locations, locations_content };
    }),
  faq: publicProcedure
    .input(
      z.object({
        id: z.string(),
        lang: z.string(),
      })
    )
    .query(async ({ ctx, input: { id, lang } }) => {
      const { data } = await ctx.axios.get(`airports/${id}/faq`, {
        params: { lang: lang },
      });

      if (data) return data;

      throw new TRPCError({ code: "NOT_FOUND" });
    }),
  details: publicProcedure
    .input(
      z.object({
        id: z.string(),
      })
    )
    .query(async ({ ctx, input: { id } }) => {
      const { data } = await ctx.axios.get(`airports/${id}/details`);

      if (data) return data;

      throw new TRPCError({ code: "NOT_FOUND" });
    }),
  pageContent: publicProcedure
    .input(
      z.object({
        devtitle: z.string(),
        airport: z.number(),
        lang: z.number(),
      })
    )
    .query(async ({ ctx, input: { devtitle, airport, lang } }) => {
      const details = await ctx.prisma.locations_pages_content.findMany({
        where: {
          location_id: airport,
          devtitle,
          lang,
        },
      });

      if (details) return details;

      throw new TRPCError({ code: "NOT_FOUND" });
    }),
  reviewStatistics: publicProcedure
    .input(
      z.object({
        lang: z.string(),
      })
    )
    .query(async ({ ctx, input: { lang } }) => {
      const statistics = await ctx.prisma.$queryRaw<{
        average_reviews: number;
        lang: string;
      }>`SELECT ROUND(AVG(parkos.reviews.rating) / 2, 1) AS average_reviews, parkos.languages.lang
        FROM parkos.reviews INNER JOIN parkos.languages
        ON parkos.languages.id = parkos.reviews.language_id AND parkos.languages.lang = ${lang};`;
      console.log(statistics);
      if (statistics) return statistics;

      throw new TRPCError({ code: "INTERNAL_SERVER_ERROR" });
    }),
  reviews: publicProcedure
    .input(
      z.object({
        limit: z.number().optional().default(50),
        airport: z.number(),
        lang: z.string(),
      })
    )
    .query(async ({ ctx, input: { limit, airport, lang } }) => {
      const meta = await ctx.prisma.$queryRaw<
        {
          score: number;
          best: number;
          worst: number;
          count: number;
        }[]
      >`SELECT ROUND(AVG(R.rating), 2) AS score,COUNT(1) AS count,ROUND(MAX(R.rating), 2) AS best,ROUND(MIN(R.rating), 2) AS worst
         FROM reviews R JOIN languages L ON L.id = R.language_id JOIN merchants M ON M.id = R.merchant_id
         JOIN reservations RES ON RES.id = R.reservation_id JOIN locations LOC ON LOC.id = M.locationID
         WHERE R.status = 'open' AND L.lang = ${lang} AND (LOC.id = ${airport} OR ${airport} IS NULL) AND R.publish = 1`;

      const reviews = await ctx.prisma.$queryRaw<
        {
          name: string;
          content: string;
          score: number;
          created_at: Date;
          arrival: Date;
          departure: Date;
          merchant: string;
          merchant_slug: string;
          airport: string;
          airport_slug: string;
          airport_content_slug: string;
          airport_title: string;
          parking_type: string;
        }[]
      >`SELECT R.name,R.message AS content,R.rating AS score,R.created_at AS date,RES.arrival,RES.departure,M.name 
         AS merchant,M.slug AS merchant_slug,L.name AS airport,L.slug AS airport_slug,LC.slug AS airport_content_slug,L.maintitle 
         AS airport_title,RES.parkingtype AS parking_type
         FROM reviews R LEFT JOIN reservations RES ON RES.id = R.reservation_id LEFT JOIN merchants M ON M.id = R.merchant_id LEFT JOIN locations L 
         ON L.Id = M.locationID JOIN languages LANG ON LANG.id = R.language_id JOIN locations_content LC ON LC.location_id = L.id 
         AND LC.lang = LANG.id WHERE (L.id = ${airport} OR ${airport} IS NULL) AND M.status = 1 AND R.status = ${status} AND R.publish = 1 
         AND LANG.lang = ${lang} ORDER BY R.created_at DESC LIMIT ${limit} OFFSET 0`;

      if (meta && reviews)
        return {
          data: {
            [lang]: reviews,
          },
          meta: {
            reviews: meta,
          },
        };
    }),
  statistics: publicProcedure
    .input(
      z.object({
        slug: z.string(),
        domain: z.string(),
        airport: z.number(),
        langId: z.number(),
      })
    )
    .query(async ({ ctx, input: { slug, domain, airport, langId } }) => {
      const statistics = await ctx.prisma.$queryRaw<{
        id: number;
        avg_price_per_day: number;
        lowest_week_price: number;
        avg_review_score: number;
        review_count: number;
        best_rated_parking: string;
        closest_parking: string;
        cheapest_parking: string;
        merchant_count: number;
      }>`SELECT LS.id,LS.avg_price_per_day,LS.lowest_week_price,LS.avg_review_score,LS.review_count,LS.best_rated_parking,LS.closest_parking,
         LS.cheapest_parking,LS.merchant_count 
         FROM location_statistics LS 
         WHERE LS.location_id = ${airport} AND LS.language_id = ${langId} LIMIT 1`;

      const merchants = await ctx.prisma.merchants.findMany({
        where: {
          locationID: airport,
          status: 1,
        },
      });

      const [closestMerchant] = merchants.filter(
        (m) => m.name === statistics?.closest_parking
      );
      const [cheapestMerchant] = merchants.filter(
        (m) => m.name === statistics?.cheapest_parking
      );
      const [bestRatedMerchant] = merchants.filter(
        (m) => m.name === statistics?.best_rated_parking
      );

      const requiredFields = ["id", "slug", "name"];
      const respObj = {
        "merchant-count": statistics?.merchant_count,
        avg_price_per_day: statistics?.avg_price_per_day,
        closest: {
          merchant: !closestMerchant
            ? null
            : stripObject(requiredFields, closestMerchant),
        },
        cheapest: {
          merchant: !cheapestMerchant
            ? null
            : stripObject(requiredFields, cheapestMerchant),
        },
        "best-rated": {
          merchant: !bestRatedMerchant
            ? null
            : stripObject(requiredFields, bestRatedMerchant),
        },
      };

      return respObj;
    }),
});
