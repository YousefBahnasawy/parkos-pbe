import { publicProcedure } from "../trpc";

export const languaugesRouter = publicProcedure.query(async ({ ctx }) => {
  const languages = await ctx.prisma.$queryRaw<
    {
      id: number;
      object: "language";
      lang: string;
      name: string;
      native_name: string;
      domain: string;
      gtm_key: string;
      extra_gtm_key: string | null;
      zendesk_lang_code: string | null;
      youtube: string;
      twitter: string;
      facebook: string;
      status: 1 | 0;
      is_minimal_product: 1 | 0;
      country_id?: number;
      country_code?: string;
      country_name?: string;
      currency_id?: number;
      currency_name?: string;
      currency_iso_code?: string;
    }[]
  >`
    SELECT languages.id, 'language' AS 'object', languages.lang, languages.name, languages.native_name, languages.domain, 
      languages.gtm_key, languages.extra_gtm_key, languages.zendesk_lang_code, languages.youtube,
      languages.twitter, languages.facebook, 
      IF(active = 1 OR active = 3, 1, 0) as status,
      IF(active = 3, 1, 0) as is_minimal_product, 
      countries.id as country_id, countries.country_code as country_code, countries.name as country_name,
      currencies.id as currency_id, currencies.name as currency_name, currencies.iso_code as currency_iso_code
    FROM languages 
    INNER JOIN countries ON countryId = countries.id
    INNER JOIN currencies ON countries.currency_id = currencies.id
    WHERE (active = 1 OR active = 3) AND is_hidden = 0
    `;

  return languages.map(
    ({
      country_id,
      country_code,
      country_name,
      currency_id,
      currency_name,
      currency_iso_code,
      ...language
    }) => {
      const lang = {
        ...language,
        id: Number(language.id),
        status: Number(language.status),
        is_minimal_product: Number(language.is_minimal_product),
        country: {
          id: Number(country_id),
          code: country_code as string,
          name: country_name as string,
        },
        currency: {
          id: Number(currency_id),
          name: currency_name as string,
          iso_code: currency_iso_code as string,
        },
      };

      return lang;
    }
  );
});
