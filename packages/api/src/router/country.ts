import { z } from "zod";
import { publicProcedure, router } from "../trpc";

export const countryRouter = router({
  statistics: publicProcedure
    .input(
      z.object({
        lang: z.number(),
      })
    )
    .query(async ({ ctx, input: { lang } }) => {
      const statistics = await ctx.prisma.location_statistics.aggregate({
        _sum: {
          merchant_count: true,
        },
        where: {
          language_id: lang,
        },
      });

      return statistics;
    }),
});
