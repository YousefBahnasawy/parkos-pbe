import * as trpc from "@trpc/server";
// import * as trpcNext from "@trpc/server/adapters/next";
import { prisma } from "@parkos-pbe/db";
import axios from "axios";

export const createContextInner = async () => {
  const axiosInstance = axios.create({ baseURL: process.env.API_BASE_URL });
  return { prisma, axios: axiosInstance };
};

export const createContext = async () => {
  return await createContextInner();
};

export type Context = trpc.inferAsyncReturnType<typeof createContext>;
